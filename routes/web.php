<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', fn () => view('welcome'));

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function () {
    Route::get('/social-posts', 'SocialPostsController@index')->name('social-posts.index');
    Route::get('/social-posts/create', 'SocialPostsController@create')->name('social-posts.create');
    Route::post('/social-posts', 'SocialPostsController@store')->name('social-posts.store');
    Route::get('/social-posts/{socialPost}/edit', 'SocialPostsController@edit')->name('social-posts.edit');
    Route::patch('/social-posts/{socialPost}', 'SocialPostsController@update')->name('social-posts.update');
    Route::delete('/social-posts/{socialPost}', 'SocialPostsController@destroy')->name('social-posts.destroy');
});
