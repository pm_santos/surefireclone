<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SocialPost;
use App\User;
use Faker\Generator as Faker;

$factory->define(SocialPost::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class),
        'content' => $faker->realText,
        'channel' => $faker->randomElement(['Facebook', 'Instagram', 'Linked In', 'Youtube']),
        'status' => $faker->randomElement(['Draft', 'For Approval', 'Scheduled', 'Published', 'Declined']),
        'image' => 'https://source.unsplash.com/256x256?nature,water&id=' . Str::uuid(),
    ];
});
