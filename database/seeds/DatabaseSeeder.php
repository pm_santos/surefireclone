<?php

use App\SocialPost;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Johnny Depp',
            'email' => 'john@example.com'
        ]);

        factory(SocialPost::class, 100)->create();
    }
}
