### Jargons
    - Unit Tests - Methods
    - Feature / End-to-End Tests - Feature

### Tooling
    - PHPUnit
    - SQLite
    - Faker
    - Laravel Collission
    - Better PHPUnit

### Workflow
    - CLI
    - Environment
    - Model Factories
    - Authentication
    - Assertions
    - Mocking

### Demo Time 🔥
