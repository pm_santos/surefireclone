@extends('layouts.app')

@section('content')
<div class="max-w-screen-xl mx-auto px-6">
    <div class="font-medium text-lg">Dashboard</div>
    <div class="mt-4">You are logged in!</div>
</div>
@endsection
