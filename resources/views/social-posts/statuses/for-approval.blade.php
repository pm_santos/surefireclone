<span class="inline-flex items-center px-2 py-1 rounded-lg bg-orange-100">
    <span class="w-2 h-2 rounded-full bg-orange-400"></span>
    <span class="ml-2 text-xs uppercase font-medium text-orange-600">{{ $socialPost->status }}</span>
</span>
