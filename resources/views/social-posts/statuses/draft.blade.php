<span class="inline-flex items-center px-2 py-1 rounded-lg bg-gray-100">
    <span class="w-2 h-2 rounded-full bg-gray-400"></span>
    <span class="ml-2 text-xs uppercase font-medium text-gray-600">{{ $socialPost->status }}</span>
</span>
