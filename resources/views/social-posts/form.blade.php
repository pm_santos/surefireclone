<div class="flex flex-wrap">
    <div class="w-full md:w-1/3 ">
        <label for="content" class="inline-block font-medium text-gray-600 mb-1">Content</label>
    </div>
    <div class="w-full md:w-2/3">
        <textarea
            class="form-input block w-full"
            id="content"
            placeholder="Write something nice"
            name="content"
            rows="7"
        >{{ old('content', $socialPost->content) }}</textarea>

        <p class="text-gray-500 mt-2">
            For twitter posts, you may only post up to 255 characters.
        </p>

        @error('content')
            <p class="text-red-500 text-sm mt-2">
                {{ $message }}
            </p>
        @enderror
    </div>
</div>

<hr class="my-8 border-gray-200">

<div class="flex flex-wrap">
    <div class="w-full md:w-1/3 ">
        <label for="channel" class="inline-block font-medium text-gray-600 mb-1">Channel</label>
    </div>
    <div class="w-full md:w-2/3">
        <select id="channel" name="channel" class="form-select block w-full max-w-sm">
            <option {{ old('channel', $socialPost->channel) === 'Facebook' ? 'selected' : '' }}>Facebook</option>
            <option {{ old('channel', $socialPost->channel) === 'Instagram' ? 'selected' : '' }}>Instagram</option>
            <option {{ old('channel', $socialPost->channel) === 'Linked In' ? 'selected' : '' }}>Linked In</option>
            <option {{ old('channel', $socialPost->channel) === 'Youtube' ? 'selected' : '' }}>Youtube</option>
        </select>

        <p class="text-gray-500 mt-2">
            You may add more channels in the <a href="#" class="text-indigo-600">settings page</a>.
        </p>

        @error('channel')
            <p class="text-red-500 text-sm mt-2">
                {{ $message }}
            </p>
        @enderror
    </div>
</div>

<hr class="my-8 border-gray-200">

<div class="flex flex-wrap">
    <div class="w-full md:w-1/3">
        <label for="status" class="inline-block font-medium text-gray-600 mb-1">Status</label>
    </div>
    <div class="w-full md:w-2/3">
        <select id="status" name="status" class="form-select block w-full max-w-sm">
            <option {{ old('status', $socialPost->status) === 'Draft' ? 'selected' : '' }}>Draft</option>
            <option {{ old('status', $socialPost->status) === 'For Approval' ? 'selected' : '' }}>For Approval</option>
            <option {{ old('status', $socialPost->status) === 'Scheduled' ? 'selected' : '' }}>Scheduled</option>
            <option {{ old('status', $socialPost->status) === 'Published' ? 'selected' : '' }}>Published</option>
            <option {{ old('status', $socialPost->status) === 'Declined' ? 'selected' : '' }}>Declined</option>
        </select>

        @error('status')
            <p class="text-red-500 text-sm mt-2">
                {{ $message }}
            </p>
        @enderror
    </div>
</div>

<hr class="my-8 border-gray-200">

<div class="flex flex-wrap">
    <div class="w-full md:w-1/3">
        <label for="image" class="inline-block font-medium text-gray-600 mb-1">Image</label>
    </div>
    <div class="w-full md:w-2/3">
        @if($socialPost->image)
            <img class="w-full max-w-sm rounded object-fit mb-4" src="{{ $socialPost->image }}" alt="">
        @endif

        <button
            @click.prevent="$refs.fileInput.click()"
            class="px-4 py-2 text-gray-700 border rounded-lg inline-flex items-center hover:bg-gray-100 focus:outline-none focus:shadow-outline-blue focus:border-blue-300"
        >
            <svg class="w-5 h-5 text-gray-400" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" stroke="currentColor" viewBox="0 0 24 24"><path d="M4 16l4.586-4.586a2 2 0 012.828 0L16 16m-2-2l1.586-1.586a2 2 0 012.828 0L20 14m-6-6h.01M6 20h12a2 2 0 002-2V6a2 2 0 00-2-2H6a2 2 0 00-2 2v12a2 2 0 002 2z"></path></svg>
            <span class="ml-2 font-medium" ref="fileLabel">Upload photo</span>
        </button>
        <input
            @change="e => $refs.fileLabel.innerHTML = e.target.files[0].name"
            accept="image/*"
            class="hidden"
            name="image"
            ref="fileInput"
            type="file"
        >

        @error('image')
            <p class="text-red-500 text-sm mt-2">
                {{ $message }}
            </p>
        @enderror
    </div>
</div>

<hr class="my-8 border-gray-200">

<div class="text-right space-x-2">
    <a href="{{ route('social-posts.index') }}" class="inline-flex items-center px-4 py-2 border font-medium rounded-lg focus:outline-none focus:border-blue-300 focus:shadow-outline-blue">Cancel</a>
    <button type="submit" class="inline-flex items-center px-4 py-2 border border-indigo-600 font-medium bg-indigo-600 text-white rounded-lg focus:outline-none focus:shadow-outline-blue">Save</button>
</div>
