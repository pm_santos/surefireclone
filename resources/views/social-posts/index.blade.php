@extends('layouts.app')

@section('content')
<div class="max-w-screen-xl mx-auto px-6">
    <div class="flex justify-between">
        <div class="flex items-center">
            <h1 class="font-medium text-lg">Social Posts</h1>
            <form class="ml-4">
                <div class="relative w-56">
                    <input name="search" value="{{ request('search') }}" class="form-input pr-10 block w-full" type="text" placeholder="Search">
                    <div class="pointer-events-none px-3 absolute right-0 inset-y-0 flex items-center">
                        <svg class="w-5 h-5 text-gray-400" fill="none" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" stroke="currentColor" viewBox="0 0 24 24"><path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path></svg>
                    </div>
                </div>
            </form>
        </div>
        <div class="flex items-center">
            <a href="{{ route('social-posts.create') }}" class="inline-flex items-center px-4 py-2 bg-indigo-600 font-medium text-white rounded-lg focus:shadow-outline">Create New</a>
        </div>
    </div>

    <div class="inline-block bg-white min-w-full rounded-lg overflow-hidden shadow mt-6">
        <table class="min-w-full">
            <thead>
                <tr class="border-b border-gray-200 bg-gray-50">
                    <th width="1%" class="pl-6 pr-3 py-3 medium text-left text-sm text-gray-600">
                        <input type="checkbox" class="form-checkbox">
                    </th>
                    <th width="25%" class="px-6 py-4 font-medium text-left text-sm text-gray-600">Content</th>
                    <th width="10%" class="px-6 py-4 font-medium text-left text-sm text-gray-600">Author</th>
                    <th width="1%" class="px-6 py-4 font-medium text-left text-sm text-gray-600">Status</th>
                    <th width="1%" class="px-6 py-4 font-medium text-left text-sm text-gray-600">Last Updated</th>
                </tr>
            </thead>
            <tbody>
                @foreach($socialPosts as $socialPost)
                <tr class="border-b border-gray-200 group hover:bg-gray-50 {{ $socialPost->is(session('socialPost')) ? 'bg-blue-50' : '' }}">
                    <td class="pl-6 pr-3 text-sm">
                        <input type="checkbox" class="form-checkbox">
                    </td>
                    <td class="px-6 py-4 text-sm">
                        <div class="flex">
                            <div class="flex-shrink-0">
                                <img class="w-10 h-8 rounded object-cover" src="{{ $socialPost->image }}" alt="">
                            </div>
                            <div class="flex-1 ml-4">
                                <div>{{ Str::limit($socialPost->content, 100) }}</div>
                                <div class="flex items-center space-x-4 mt-2">
                                    <span class="inline-flex items-center">
                                        <img class="w-4 h-4" src="{{ asset('img/' . Str::slug($socialPost->channel, '-') . '.svg') }}" alt="">
                                        <span class="ml-2">{{ $socialPost->channel }}</span>
                                    </span>
                                    <a href="{{ route('social-posts.edit', $socialPost) }}" class="invisible group-hover:visible font-medium">Edit</a>
                                    <a href="{{ route('social-posts.destroy', $socialPost) }}" class="invisible group-hover:visible font-medium js-confirm-delete">Delete</a>
                                </div>
                            </div>
                        </div>
                    </td>
                    <td class="px-6 py-4 text-sm whitespace-no-wrap">
                        <div class="flex items-center">
                            <img class="w-8 h-8 rounded-full" src="{{ $socialPost->author->avatar }}" alt="">
                            <div class="ml-4">
                                <div>{{ $socialPost->author->name }}</div>
                                <div class="text-gray-500">{{ $socialPost->author->email }}</div>
                            </div>
                        </div>
                    </td>
                    <td class="px-6 py-4 text-sm whitespace-no-wrap">
                        @include('social-posts.statuses.' . Str::slug($socialPost->status, '-'))
                    </td>
                    <td class="px-6 py-4 text-sm whitespace-no-wrap">
                        <span class="text-gray-700">{{ $socialPost->updated_at->diffForHumans() }}</span>
                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr class="border-t">
                    <td colspan="5" class="px-6 py-4">
                        <div class="flex items-center justify-between">
                            <div class="text-sm text-gray-700">Showing {{ $socialPosts->firstItem() }} to {{ $socialPosts->lastItem() }} of {{ $socialPosts->total() }} items</div>
                            <div>
                                {{ $socialPosts->withQueryString()->links('vendor.pagination.simple-default') }}
                            </div>
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
@endsection

@push('scripts')
<form id="deleteForm" method="POST">
    @csrf
    @method('DELETE')
</form>

<script>
    (function() {
        document.addEventListener('click', function ($event) {
            if ($event.target.className.includes('js-confirm-delete')) {
                $event.preventDefault();
                if (window.confirm('Are you really really sure?')) {
                    let deleteForm = document.getElementById('deleteForm')
                    deleteForm.setAttribute('action', $event.target.getAttribute('href'))
                    deleteForm.submit()
                }
            }
        });
    })()
</script>
@endpush
