@extends('layouts.app', ['bodyClass' => 'bg-white'])

@section('content')
<div class="max-w-screen-md mx-auto px-6">
    <h1 class="font-medium text-lg">Edit this post</h1>
    <p class="text-gray-500 mt-1">You may modify any information for this social post as long as the status is neither scheduled nor published.</p>
    <hr class="my-8 border-gray-200">
    <form action="{{ route('social-posts.update', $socialPost) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PATCH')

        @include('social-posts.form')
    </form>
</div>
@endsection
