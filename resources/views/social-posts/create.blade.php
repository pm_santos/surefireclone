@extends('layouts.app', ['bodyClass' => 'bg-white'])

@section('content')
<div class="max-w-screen-md mx-auto px-6">
    <h1 class="font-medium text-lg">Write a social post</h1>
    <p class="text-gray-500 mt-1">Start writing your social post by providing the details below.</p>
    <hr class="my-8 border-gray-200">
    <form action="{{ route('social-posts.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        @include('social-posts.form', ['socialPost' => new App\SocialPost])
    </form>
</div>
@endsection
