@if ($paginator->hasPages())
    <nav>
        <ul class="flex items-center space-x-2">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="px-3 py-2 border rounded-lg text-sm text-gray-400" aria-disabled="true"><span>Previous</span></li>
            @else
                <li><a class="px-3 py-2 border rounded-lg text-sm text-gray-700 hover:bg-gray-100" href="{{ $paginator->previousPageUrl() }}" rel="prev">Previous</a></li>
            @endif

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li><a class="px-3 py-2 border rounded-lg text-sm text-gray-700 hover:bg-gray-100" href="{{ $paginator->nextPageUrl() }}" rel="next">Next</a></li>
            @else
                <li class="px-3 py-2 border rounded-lg text-sm text-gray-400" aria-disabled="true"><span>Next</span></li>
            @endif
        </ul>
    </nav>
@endif
