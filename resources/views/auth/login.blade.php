@extends('layouts.app')

@section('content')
<div class="max-w-sm mx-auto mt-24">
    <h1 class="font-semibold text-lg text-center mb-8">Login to your account</h1>
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div>
            <label for="email" class="inline-block text-sm mb-1">{{ __('Email Address') }}</label>

            <div>
                <input
                    id="email"
                    type="email"
                    class="form-input w-full block"
                    name="email"
                    value="{{ old('email') }}"
                    required
                    autocomplete="email"
                    autofocus>

                @error('email')
                    <p class="text-sm text-red-500 mt-2" role="alert">{{ $message }}</p>
                @enderror
            </div>
        </div>

        <div class="mt-4">
            <label for="password" class="inline-block text-sm mb-1">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input
                    id="password"
                    type="password"
                    class="form-input block w-full"
                    name="password"
                    required
                    autocomplete="current-password">

                @error('password')
                    <p class="text-sm text-red-500 mt-2" role="alert">{{ $message }}</p>
                @enderror
            </div>
        </div>

        <div class="flex justify-between mt-4">
            <label class="inline-flex items-center">
                <input
                    class="form-checkbox"
                    type="checkbox"
                    name="remember"
                    id="remember"
                    {{ old('remember') ? 'checked' : '' }}>

                <span class="ml-2">
                    {{ __('Remember Me') }}
                </span>
            </label>

            <a href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a>
        </div>

        <div class="mt-6">
            <button type="submit" class="block w-full px-4 py-2 rounded-lg text-white bg-indigo-600 font-medium">
                {{ __('Login') }}
            </button>
        </div>
    </form>
</div>
@endsection
