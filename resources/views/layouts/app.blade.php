<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body class="font-sans antialiased {{ $bodyClass ?? 'bg-gray-200 bg-opacity-50' }}">
    <div id="app">
        <div class="h-16 bg-white border-b flex items-center">
            <div class="w-full max-w-screen-xl mx-auto px-6 flex items-center justify-between">
                <div class="inline-flex items-center">
                    <a href="{{ url('/') }}" class="font-semibold">Surefire Clone</a>
                </div>

                @auth
                <div class="inline-flex items-center space-x-8">
                    <a class="font-medium text-gray-400 hover:text-gray-700" href="#">Leads</a>
                    <a class="font-medium text-gray-400 hover:text-gray-700" href="#">Reputation</a>
                    <a class="font-medium {{ Route::is('social-posts*') ? 'text-current' : 'text-gray-400' }} hover:text-gray-700" href="{{ route('social-posts.index') }}">Social</a>
                </div>
                @endauth

                <div class="inline-flex items-center space-x-4">
                    @guest
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @else
                        <div class="relative group">
                            <a href="" class="flex items-center">
                                <img class="w-6 h-6 rounded-full" src="{{ Auth::user()->avatar }}" alt="">
                                <span class="ml-2 font-medium text-gray-700">{{ Auth::user()->name }}</span>
                            </a>
                            <div class="invisible group-hover:visible absolute right-0 w-56 pt-2">
                                <div class="rounded-lg shadow-lg">
                                    <div class="bg-white rounded-lg shadow-xs">
                                        <div class="py-1">
                                            <a href="" class="block w-full px-4 py-2 text-gray-700 text-sm hover:bg-gray-200">Profile</a>
                                            <a href="" class="block w-full px-4 py-2 text-gray-700 text-sm hover:bg-gray-200">Change Password</a>
                                            <hr class="my-1">
                                            <a href="" class="block w-full px-4 py-2 text-gray-700 text-sm hover:bg-gray-200">Contact Support</a>
                                            <a href="#" onclick="document.getElementById('logoutForm').submit()" class="block w-full px-4 py-2 text-gray-700 text-sm hover:bg-gray-200">Logout</a>
                                            <form id="logoutForm" class="hidden" action="{{ route('logout') }}" method="POST">
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endguest
                </div>
            </div>
        </div>

        <main class="py-8">
            @yield('content')
        </main>
    </div>

    @stack('scripts')
</body>
</html>
