<?php

namespace Tests\Feature;

use App\SocialPost;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SocialPostsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_view_all_posts()
    {
        $this->actingAs(factory(User::class)->create());

        $this->get('/social-posts')->assertSuccessful();
    }

    /** @test */
    public function a_user_can_search_posts()
    {
        $this->actingAs(factory(User::class)->create());

        factory(SocialPost::class)->create(['content' => 'Searchable content']);
        factory(SocialPost::class)->create(['content' => 'Dont see this content']);

        $this->get('/social-posts?search=Searchable')
            ->assertSee('Searchable content')
            ->assertDontSee('Dont see this content');
    }

    /** @test */
    public function a_user_can_view_create_post_page()
    {
        $this->actingAs(factory(User::class)->create());

        $this->get('/social-posts/create')->assertSuccessful();
    }

    /** @test */
    public function a_user_can_create_a_post()
    {
        $this->actingAs(factory(User::class)->create());

        $values = factory(SocialPost::class)->raw([
            'image' => null,
            'user_id' => auth()->id(),
        ]);

        $this->post('/social-posts', $values);

        $this->assertDatabaseHas('social_posts', $values);
    }

    /** @test */
    public function a_user_can_view_edit_post_page()
    {
        $this->actingAs(factory(User::class)->create());

        $socialPost = factory(SocialPost::class)->create();

        $this->get('/social-posts/' . $socialPost->id . '/edit')->assertSuccessful();
    }

    /** @test */
    public function a_user_can_update_a_post()
    {
        $this->actingAs(factory(User::class)->create());

        $socialPost = factory(SocialPost::class)->create([
            'user_id' => auth()->id(),
        ]);

        $values = factory(SocialPost::class)->raw([
            'image' => null,
            'user_id' => auth()->id(),
        ]);

        $this->patch('/social-posts/' . $socialPost->id, $values);

        $this->assertDatabaseHas('social_posts', $values);
    }

    /** @test */
    public function a_user_can_delete_a_post()
    {
        $this->actingAs(factory(User::class)->create());

        $socialPost = factory(SocialPost::class)->create([
            'user_id' => auth()->id(),
        ]);

        $this->delete('/social-posts/' . $socialPost->id);

        $this->assertDatabaseMissing('social_posts', ['id' => $socialPost->id]);
    }
}
