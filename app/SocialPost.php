<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialPost extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The author associated to this post.
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
