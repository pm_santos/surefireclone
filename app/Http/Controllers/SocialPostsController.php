<?php

namespace App\Http\Controllers;

use App\SocialPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class SocialPostsController extends Controller
{
    public function index()
    {
        return view('social-posts.index', [
            'socialPosts' => SocialPost::with('author')
                ->where('content', 'like', '%' . request('search') . '%')
                ->orderByDesc('updated_at')
                ->paginate()
        ]);
    }

    public function create()
    {
        return view('social-posts.create');
    }

    public function store(Request $request)
    {
        $attributes = $request->validate([
            'content' => ['required', 'string'],
            'channel' => ['required', Rule::in('Facebook', 'Instagram', 'Linked In', 'Youtube')],
            'status' => ['required', Rule::in('Draft', 'For Approval', 'Scheduled', 'Published', 'Declined')],
            'image' => ['nullable', 'image'],
        ]);

        if ($request->hasFile('image')) {
            $attributes['image'] = Storage::url(
                $request->file('image')->store('uploads', 'public')
            );
        }

        $socialPost = SocialPost::create(array_merge($attributes, [
            'user_id' => auth()->id()
        ]));

        return redirect()
            ->route('social-posts.index')
            ->with(compact('socialPost'));
    }

    public function edit(SocialPost $socialPost)
    {
        return view('social-posts.edit', compact('socialPost'));
    }

    public function update(Request $request, SocialPost $socialPost)
    {
        $attributes = $request->validate([
            'content' => ['required', 'string'],
            'channel' => ['required', Rule::in('Facebook', 'Instagram', 'Linked In', 'Youtube')],
            'status' => ['required', Rule::in('Draft', 'For Approval', 'Scheduled', 'Published', 'Declined')],
            'image' => ['nullable', 'image'],
        ]);

        if ($request->hasFile('image')) {
            $attributes['image'] = Storage::url(
                $request->file('image')->store('uploads', 'public')
            );
        }

        $socialPost->update($attributes);

        return redirect()
            ->route('social-posts.index')
            ->with(compact('socialPost'));
    }

    public function destroy(SocialPost $socialPost)
    {
        $socialPost->delete();

        return redirect()->route('social-posts.index');
    }
}
